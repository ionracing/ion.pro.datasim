﻿using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ion.Pro.DataSim
{
    public partial class SimForm : Form
    {
        LocalNetworkClient simClient;
        public SimForm()
        {
            InitializeComponent();
            CanError.Initialize();
            simClient = new LocalNetworkClient(161);
            simClient.Connect(IPAddress.Loopback, 160);
            /*simClient.SendDataWrappers(
                new DataWrapper[]
                {
                    new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.SPEED).ID, Value = 100 },
                    new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.VOLTAGE12).ID, Value = 13 },
                    new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.SOC).ID, Value = 10 },
                    new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.TEMPBAT).ID, Value = 1 },
                    new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.TEMPCOOL).ID, Value = 1 }
                });*/
            foreach (CanError error in CanError.AllErrors())
            {
                listBox1.Items.Add(new ObjContainer<CanError>(error, (s) => $"{s.SensorID}.{s.ValueID}: {s.Msg}"));
            }
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            simClient.SendDataWrappers(
            new DataWrapper[]
            {
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.SPEED).ID, Value = e.NewValue },
            });
        }

        private void vScrollBar2_Scroll(object sender, ScrollEventArgs e)
        {
            simClient.SendDataWrappers(
            new DataWrapper[]
            {
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.VOLTAGE12).ID, Value = e.NewValue },
            });
        }

        private void vScrollBar3_Scroll(object sender, ScrollEventArgs e)
        {
            simClient.SendDataWrappers(
            new DataWrapper[]
            {
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.SOC).ID, Value = e.NewValue },
            });
        }

        private void vScrollBar4_Scroll(object sender, ScrollEventArgs e)
        {
            simClient.SendDataWrappers(
            new DataWrapper[]
            {
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.TEMPBAT).ID, Value = e.NewValue },
            });
        }

        private void vScrollBar5_Scroll(object sender, ScrollEventArgs e)
        {
            simClient.SendDataWrappers(
            new DataWrapper[]
            {
                new DataWrapper() { SensorID = SensorLookup.GetByName(SensorLookup.TEMPCOOL).ID, Value = e.NewValue }
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CanError error = (listBox1.SelectedItem as ObjContainer<CanError>)?.Value;
            if (error != null)
            {
                simClient.SendDataWrappers(
                new DataWrapper[]
                {
                    new DataWrapper() { SensorID = (ushort)error.SensorID, Value = error.ValueID }
                });
            }
        }
    }

    public class ObjContainer<T>
    {
        public T Value { get; set; }
        public Func<T, string> ToStringOverride { get; set; }

        public ObjContainer(T value, Func<T, string> toStringOverride)
        {
            Value = value;
            ToStringOverride = toStringOverride;
        }


        public override string ToString()
        {
            return ToStringOverride(Value);
        }

    }
}
