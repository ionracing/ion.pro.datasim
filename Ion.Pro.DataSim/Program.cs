﻿using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ion.Pro.DataSim
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {


            Application.EnableVisualStyles();
            Application.Run(new SimForm());
        }
    }
}
